<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ProTech 2018
 */

get_header('design'); 
get_template_part('template-parts/header/single-header'); ?>

	<div class="content-area ">
		<main id="main" class="site-main">
        <div class="wrap">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'single' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
          </div>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>

<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ProTech 2018
 */

?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>">

	<?php wp_head();?>

</head>

<body <?php body_class();?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e('Skip to content', 'protech');?></a>

	<header class="site-header">
		<div class="wrap">
			<button type="button" class="off-canvas-open" aria-expanded="false" aria-label="<?php esc_html_e('Open Menu', 'protech');?>">
				<span class="hamburger"></span>
			</button>
			<div class="brand-name">
			<a class="navbar-brand page-scroll" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg/logo.svg'; ?>" ></a>
			</div>
			<nav id="site-navigation" class="main-navigation">
				<?php
					wp_nav_menu(array(
						'theme_location' => 'primary',
						'menu_id' => 'primary-menu',
						'menu_class' => 'menu dropdown',
					));
				?>
			</nav><!-- #site-navigation -->
		</div><!-- .wrap -->
	</header><!-- .site-header-->
	<div id="content" class="site-content">


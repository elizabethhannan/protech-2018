<?php
/**
 * Template Name: Page with Content Blocks
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

get_header(); 
get_template_part('header-after'); 
?>

	<div class="content-area">
		<main id="main" class="site-main">
		<?php ptig_display_content_blocks(); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>

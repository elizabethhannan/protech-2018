<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

?>

<article id="post-<?php the_ID();?>" <?php post_class();?> style="background: <?php the_field('background_col');?>">
		<div class="entry-content odd" style="background-image: url('<?php the_field('background_img')?>;">
			<div class="site-description spaceright" style="color: <?php the_field('content_color')?>;">
			<h4 class="sit-name"><?php the_field('site_name') ?></h4>
			<?php the_title('<h3 class="entry-title odd"><a style="color:' .  get_field('content_color') .  '>;" href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');?>
			<p><?php the_field('excerpt');?></p>
			<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark" class="button round primary-button">View Project</a>
			</div>

		<div class="screenshot">
		<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark"><img src="<?php the_field('site_image');?>"></a>
		</div>
		</div><!-- .entry-content -->
</article><!-- #post-## -->



<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

?>

<article class="blog-post"<?php post_class();?>>
<?php
if (has_post_thumbnail()) {?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
			<?php
the_post_thumbnail('blog_grid');
    ?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php }?>
	<div class="entry-meta">
			 <?php echo get_the_category()[0]->name; ?>
		</div><!-- .entry-meta -->
	<header class="entry-header">
		<?php
if (is_single()):
    the_title('<h1 class="entry-title">', '</h1>');
else:
    the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
endif;
if ('post' === get_post_type()):
?>
		<?php endif;?>
	</header><!-- .entry-header -->
<div class="entry-content">
 <p class="author"><?php echo get_avatar(get_the_author_meta('ID'), 32); ?>Written by: <strong><?php echo get_the_author_meta('user_nicename', $post->post_author); ?></strong></p>
<?php
wp_link_pages(array(
    'before' => '<div class="page-links">' . esc_html__('Pages:', 'protech'),
    'after' => '</div>',
));
?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->


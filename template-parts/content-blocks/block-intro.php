<section class="intro" style="background: url('<?php the_sub_field('background_image'); ?>')">
  <div class="wrap">
		<div class="heading">
			<h1 class="title"><?php the_sub_field('title');?></h1>

			<p class="content"><?php the_sub_field('content');?></p>
			
			<a class="button intro-button round primary-button" href="<?php the_sub_field('button_link');?>"><?php the_sub_field('button');?></a>
		</div>
  </div>
</section>
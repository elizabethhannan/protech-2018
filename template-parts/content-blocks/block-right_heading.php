<div class="row block-image_right">

<div class="col-12 col-md-6 d-flex align-items-center justify-content-center">
   <img src="<?php the_sub_field('himage_left'); ?>">
</div>

<div class="col-12 col-md-6 ">
   <h1> <?php the_sub_field('rtitle'); ?></h1>
   <p> <?php  the_sub_field('rimage'); ?></p>
</div>
</div>

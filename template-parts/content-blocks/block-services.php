
<section class="services" style="background-color:<?php the_sub_field('background_color');?>">
        <div class="wrap">
			<h2 class="service-title"><?php the_sub_field('header');?></h2>
			<p class="service-intro"><?php the_sub_field('content');?></p>
            <div class="our-services">
                <?php if (have_rows('services')): ?>

				<?php while (have_rows('services')): the_row();?>
	                <div class="flex-item service text-center">
					   <img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
	                    <h3>
						   <?php the_sub_field('title');?>
	                    </h3>
						<p><?php the_sub_field('content');?> </p>
	                        <a class="button secondary" href="<?php the_sub_field('button_link');?>">
	                            <?php the_sub_field('button');?>
	                        </a>
	                </div>
	                <?php endwhile;?>
                <?php endif;?>
            </div>
        </div>
	</section>
	


<section class="single-portfolio-content">
<div class="wrap">
	<div class="image">
	<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
	</div>
	<div class="contents">
        <h3>
          <?php the_sub_field('title');?>
        </h3>
        <p>
         <?php the_sub_field('content');?>
		</p>
		<div class="project-btn">
		<a href="<?php the_sub_field('button_link') ?>" class="special-button view-btn"><?php the_sub_field('button');?></a>
		</div>
	</div>
</div>
</section>

<div class="wrap">
        <div class="Services">
		<?php if( have_rows('right_image') ): ?>

		<?php while( have_rows('right_image') ): the_row();
			?>
            <div class="service-content">
                <h3>
                    <?php the_sub_field('title'); ?>
                </h3>
                <p>
                    <?php the_sub_field('service_content'); ?>
                </p>
            </div>
            <div class="right-image">
                <img src="<?php echo the_sub_field('right_image'); ?>">
			</div>
		<?php endwhile; ?>
        <?php endif; ?>
       </div>
</div>

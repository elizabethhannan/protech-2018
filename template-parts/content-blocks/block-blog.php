<div class="featured-blog">
       <h1>BLOG</h1>
	<div class="wrap">
<?php

// WP_Query arguments
$args = array(

    'posts_per_page' => '3',
    'order' => 'DESC',
    'orderby' => 'id',
);

// The Query
$blog = new WP_Query($args);

// The Loop
if ($blog->have_posts()) {
    while ($blog->have_posts()) {
        $blog->the_post();
        // do something
        ?>

		<div class="blog-item">
		<div class="featured-img">
		<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark"><?php the_post_thumbnail('blog_grid');?></a>
	      </div>
		<div class="entry-meta">
			<?php echo get_the_category()[0]->name; ?>
		</div><!-- .entry-meta -->
		<?php the_title('<h3 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');?>
		 <div class="feature-content">
	     <div class="author">
		 <?php echo get_avatar(get_the_author_meta('ID'), 32); ?>
		 <p>By <strong><?php echo get_the_author_meta('user_nicename', $post->post_author); ?></strong></p>
	     </div>
	</div>
	</div>
<?php
}
} else {
    // no posts found
}

// Restore original Post Data
wp_reset_postdata();
?>
</div>
</div>

<section class="quote" style="background-color:<?php the_sub_field('background_color');?>">
	<div class="wrap">
		<div class="Quote-area">
			<!--display a sub field value-->
			<p style="color:<?php the_sub_field('text_color');?>"><?php the_sub_field('content_text');?></p>
			<a href="<?php the_sub_field('button_link') ?>" class="special-button getbutton"><?php the_sub_field('button');?></a>
		</div>
	</div>
</section>

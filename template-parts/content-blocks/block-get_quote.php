<section class="quote" style="background-color:<?php the_sub_field('background_col-block');?>">
	<div class="wrap">
		<div class="Quote-area">
			<!--display a sub field value-->
			<p style="color:<?php the_sub_field('paragraph_color');?>"><?php the_sub_field('for_information');?></p>
			<a href="<?php the_sub_field('button_link') ?>" class="special-button view-btn"><?php the_sub_field('button');?></a>
		</div>
	</div>
</section>

<section class="team" style="background-color:<?php the_sub_field('background_color')?>">
<div class="wrap">
	<h2 class="team-header">
		<?php the_sub_field('header');?>
	</h2>
	<!--member Here-->
    <div class="members-list">
		<?php
         // check if the repeater field has rows of data
         if (have_rows('members')):
          // loop through the rows of data
             while (have_rows('members')): the_row();?>
		        <div class="team-member">
				<!--display a sub field value-->
					<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
					<h2><?php the_sub_field('name'); ?></h2>
					<p><?php the_sub_field('skill'); ?></p>
				</div>
		    <?php
                 endwhile;
			 else:
		       // no rows found
             endif;

            ?>
	</div>
</div>
</section>

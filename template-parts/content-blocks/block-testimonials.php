<section class="testimonials" style="background-color: <?php the_sub_field('background_testimonials');?>">
 <div class="wrap">
	    <div class="contributors">
			<div class="opinion">
				<div class="quote-image"><img src="<?php the_sub_field('quote_image')['url'];?>" alt="<?php echo the_sub_field('quote-image')['alt']; ?>" /></div>
				 <p><?php the_sub_field('content');?></p>
			</div>
			<div class="client">
				<div class="client-img">
					<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
				</div>
				<div class="details">
					<p class="name"><?php the_sub_field('name');?></p>
					<p class="title"><?php the_sub_field('position');?></p>
				</div>
			</div>
	    </div>
 </div>
</section>


<?php
$postid = get_sub_field('portfolio');
?>
<div class="featured-project">
	<article  class="background-color" id="post-<?php the_ID();?>" <?php post_class();?> style="background: <?php the_field('background_color', $postid);?>">
		<div class="entry-content" style="background-image: url('<?php the_field('background_image', $postid)?>');">
			<div class="site-description " style="color: <?php the_field('content_color', $postid)?>;">
			    <h4 class="sit-name"><?php echo the_field('site_name', $postid) ?></h4>
				<h3 class="entry-title"><a style="color:<?php the_field('content_color', $postid)?>;"  href="<?php get_the_permalink($postid);?>"><?php echo get_the_title($postid); ?></a></h3>
				<p class="f-content"> <?php echo the_field('excerpt', $postid); ?></p>
				<a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark" class="button round primary-button">View Project</a>
			</div>
			<div class="screenshot">
				<img src="<?php the_field('site_image', $postid);?>">
			</div>
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
</div>

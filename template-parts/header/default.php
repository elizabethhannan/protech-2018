
<div class="archive-name defualt-header wrap" style="background-image: url('<?php
if(get_post_type() =='portfolio'){
 echo the_field('portfolio_background_image', 'options');
} elseif(is_home()){
	echo the_field('blog_background_image', 'options');
} else {
	echo the_field('background_image');
}
 ?>');">
	<div class="wrap" >
		<?php if(is_archive()) {
			if(get_post_type() == 'portfolio') {
				echo "<h1>Our Work</h1>";
			} else {
			the_archive_title('<h1>', '</h1>');
			}
		} elseif(is_home()) {
			echo '<h1>' . 'The Blog' . '</h1>';
		} else {
			the_title('<h1>', '</h1>');
		} ?>
	</div><!-- .wrap -->
</div>


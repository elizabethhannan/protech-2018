<div class="top-hero" style="background-color: <?php the_field('background_color');?>" >
<div class="archive-name">
	<div class="wrap" style="background-image: url('<?php the_field('background_image')?>');">
<header class="entry-header ">
<div class="entry-meta">
	 <strong><?php echo get_the_category()[0]->name; ?></strong>
</div><!-- .entry-meta -->

<h2 class="single-title"><?php the_title();?></h2>

<div class="entry-meta">
			<p>Posted on: <?php echo get_the_date('n/d/y', $post->ID) ?></p>
			<p>Written by: <strong><?php echo get_the_author_meta('user_nicename', $post->post_author); ?></p></strong>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->
	</div>
		</div>
</div>


<div class="archive-name">
	<div class="wrap" style="background-image: url('<?php the_field('background_image')?>');">
		<?php if( is_single() && get_post_type() == 'portfolio') {
			//echo '<span class="portfolio-category">' . get_the_category()[0]->name . '</span>';
		} ?>
		<h1 class="header-title"><?php echo the_title(); ?></h1>
		<p class="header-intro"><?php the_sub_field('subtitle')?></p>
	</div>
</div>

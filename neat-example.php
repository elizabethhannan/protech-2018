<?php
/**
 * Template Name: Neat Example
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

get_header();?>

	<div class="primary content-area">
		<main id="main" class="site-main">
		<div class="wrap">
			<div class="intro">
				12 column div
			</div>
		</div>
		<div class="wrap">
			<div class="half">
				One Half
			</div>
			<div class="half">
				One Half
			</div>
		</div>
		<div class="wrap">
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
			<div class="quarter">One Quarter</div>
		</div>
		<div class="wrap">
			<div class="nested">
				<div class="left">
					Left
				</div>
				<div class="right">
					Right
					<div class="wrap">
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
						<div class="fourth">One Quarter</div>
					</div>
				</div>
			</div>

		</div>

		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer();?>

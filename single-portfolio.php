<?php
/**
 * The template name: single portfolio.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ProTech 2018
 */

get_header('design');
get_template_part('header-after'); 
?>

<div class="primary content-area">
	<main id="main" class="site-main">

	<?php
	if( have_rows('portfolio_content')):
		while(have_rows('portfolio_content')): the_row();
		get_template_part('template-parts/content-blocks/block-' . get_row_layout() );
		endwhile;
	endif;

	?>
	</main><!-- #main -->
</div><!-- .primary -->

<?php get_footer(); ?>

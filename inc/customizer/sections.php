<?php
/**
 * Customizer sections.
 *
 * @package ProTech 2018
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ptig_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ptig_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'protech' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ptig_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'protech' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'protech' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ptig_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'protech' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ptig_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'protech' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ptig_customize_sections' );

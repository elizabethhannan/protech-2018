<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

get_header('design'); 
get_template_part('template-parts/header/default'); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
		<?php if ( have_posts() ) : ?>
		<?php $i = 1; ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
				if( ($i % 2) === 0) {
					// Even
					get_template_part( 'template-parts/content', 'portfolio-even');
				} else {
					// Odd
					get_template_part( 'template-parts/content', 'portfolio-odd');
				}
				$i++;
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- .primary -->
<?php get_footer(); ?>

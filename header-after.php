<?php
/**
 * Controls the hero section of every page
 */

 if( have_rows('header')):
	while( have_rows('header')): the_row(); ?>
		<div class="top-hero" style="background-color: <?php the_field('background_color'); ?>" >
			<?php get_template_part('template-parts/header/block-' . get_row_layout()); ?>
		</div>

		<?php
	endwhile;
else:
	get_template_part('template-parts/header/default');

endif;
